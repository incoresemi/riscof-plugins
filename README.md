# riscof-plugins

Contains various model plugins to be used along with RISCOF (https://gitlab.com/incoresemi/riscof)

Each plugin contains a `README.md` file for details on setup, usage and configuration.


## Third-Party Plugins

| Core/DUT | Plugin | Documentation |
|:---------|:-------|:--------------|
| [neorv32](https://github.com/stnolting/neorv32) (GitHub) | [plugin-neorv32](https://github.com/stnolting/neorv32-riscof/tree/main/plugin-neorv32) (GitHub) | [github.com/stnolting/neorv32-riscof](https://github.com/stnolting/neorv32-riscof)
